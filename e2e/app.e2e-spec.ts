import { DronacharyaPage } from './app.po';

describe('dronacharya App', function() {
  let page: DronacharyaPage;

  beforeEach(() => {
    page = new DronacharyaPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
