import { Component } from '@angular/core';
import { ProductService } from './products.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ProductService]
})
export class AppComponent {
  title:string = 'Dronacharya : The Dron Solution';
  imageURL= 'http://www.topdronesforsale.org/wp-content/uploads/2015/07/Drone-Biggest-768x537.jpg';  
  isValid=true;
  onClickMe($event){
    console.log("Clicked", $event);
  }
}